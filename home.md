# Pninim Project

Promote publishing of traditional Jewish scriptures under a free license ([GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.en.html), version 1.3).

Copyright Pninim Project <https://pninim.org> (website under construction!...)

## Work in progress

[List of Books](https://gitlab.com/pninim.org/pninim/wikis/):

- [שו"ת שאילת יעבץ](https://gitlab.com/pninim.org/pninim/wikis/%D7%A9%D7%90%D7%99%D7%9C%D7%AA_%D7%99%D7%A2%D7%91%D7%A5/%D7%90/-%D7%A9%D7%A2%D7%A8)
